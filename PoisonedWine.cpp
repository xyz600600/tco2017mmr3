#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <sstream>
#include <set>

using namespace std;

const bool debug = true;

#define TEST

#ifdef TEST
class PoisonTest {
public:
  static vector<int> useTestStrips(vector<string> tests)
  {
	cout << "?" << endl;
	cout << tests.size() << endl;
	for (int i = 0; i < (int)tests.size(); ++i)
	  cout << tests[i] << endl;
	cout.flush();

	int n;
	cin >> n;
	vector<int> res(n);
	for (int i = 0; i < n; ++i)
	  cin >> res[i];
	return res;
  }
};
#endif

// -------8<------- start of solution submitted to the website -----8<-------

struct Cluster
{
public:
  // inclusive
  int from;
  // exclusive
  int to;
  
  int numPoison;
  int numUncertain;

  // for evaluation
  double eval;
  int bps;

  // for update
  int parentIndex;
  
  Cluster(int from, int to);
  Cluster(int from, int to, int numPoison);
  
  void split(int size, int num, vector<Cluster> &chunk, int parentIndex);
  int size() const;
}; 

Cluster::Cluster(int from, int to)
  : from(from)
  , to(to)
  , numPoison(0)
  , numUncertain(to - from){}

int Cluster::size() const
{
  return to - from;
}

void Cluster::split(int size, int num, vector<Cluster> &chunk, int parentIndex)
{
  const int allNum = min(to - from, size * num);

  for (int i = 0; i < num; i++)
  {
	Cluster c(from + i * allNum / num, from + (i + 1) * allNum / num);
	c.parentIndex = parentIndex;
	chunk.push_back(c);
  }
  from += allNum;
  numUncertain -= allNum;
  
  if (from == to)
  {
	numPoison = 0;
	numUncertain = 0;
  }
}

bool operator==(const Cluster &c1, const Cluster &c2)
{
  return c1.numPoison == c2.numPoison && c1.numUncertain == c2.numUncertain;
}

bool operator<(const Cluster &c1, const Cluster &c2)
{
  if (c1.numUncertain != c2.numUncertain)
  {
	return c1.numUncertain > c2.numUncertain;
  }
  else if (c1.numPoison != c2.numPoison)
  {
	return c1.numPoison > c2.numPoison;
  }
  else
  {
	return false;
  }
}

// ====================
struct Environment
{
  // constと思っている
  int testRounds;
  int numPoison;
  int numBottles;
  
  // non-constだと思ってる
  int testStrips;
  int numUncertain;
  int numDetectedPoison;
  int numNonPoison;

  double testStrips_d;
  double numUncertain_d;
  double numDetectedPoison_d;
  double numNonPoison_d;

  void setupD();
  
  Environment(const int _testRounds, const int _numPoison, const int _numBottles, const int _testStrips);
  Environment(const Environment &src);
  Environment();

  void operator=(const Environment &src);
};

void Environment::setupD()
{
  testStrips_d = testStrips;
  numUncertain_d = numUncertain;
  numDetectedPoison_d = numDetectedPoison;
  numNonPoison_d = numNonPoison;
}

Environment::Environment(){}

Environment::Environment(const int _testRounds, const int _numPoison, const int _numBottles, const int _testStrips)
  : testRounds(_testRounds)
  , numPoison(_numPoison)
  , numBottles(_numBottles)
  , testStrips(_testStrips)
{
  numUncertain = numBottles;
  numDetectedPoison = 0;
  numNonPoison = 0;

  setupD();

}

Environment::Environment(const Environment &src)
  : testRounds(src.testRounds)
  , numPoison(src.numPoison)
  , numBottles(src.numBottles)
  , testStrips(src.testStrips)
  , numUncertain(src.numUncertain)
  , numDetectedPoison(src.numDetectedPoison)
  , numNonPoison(src.numNonPoison)
  , testStrips_d(src.testStrips_d)
  , numUncertain_d(src.numUncertain_d)
  , numDetectedPoison_d(src.numDetectedPoison_d)
  , numNonPoison_d(src.numNonPoison_d){}

void Environment::operator=(const Environment &src)
{
  testRounds = src.testRounds;
  numPoison = src.numPoison;
  numBottles = src.numBottles;
	
  testStrips = src.testStrips;
  numUncertain = src.numUncertain;
  numDetectedPoison = src.numDetectedPoison;

  testStrips_d = src.testStrips_d;
  numUncertain_d = src.numUncertain_d;
  numDetectedPoison_d = src.numDetectedPoison_d;
  numNonPoison_d = src.numNonPoison_d;
}

// ====================

class PoisonedWine {
public:
  vector<int> testWine(int _numBottles, int _testStrips, int _testRounds, int _numPoison);
  
private:
  void initialize(int _numBottles, int _testStrips, int _testRounds, int _numPoison);

  // [from, to)の区間で実験をして，結果を返す
  void throwStrips(const vector<Cluster> &ranges, vector<int> &results);

  pair<int, double> selectBps(const Cluster &c, const int clusterNum, const int restRound, const Environment &e, const int depth);
  double evaluate(const Cluster &c, const int bps, const int restRound, const int depth, const Environment &e, const int clusterNum);

  double getProb(const int n, const int m, const int p);

  double nCkLog(const int n, const int k);

  bool finish(const vector<Cluster> &chunk);

  Environment env;

  vector<Cluster> nonPoisonClusters;
  
  const int NO_POISON = 0;
  const int POISON_DETECTED = 1;

  array<double, 10001> logAccum;
};

bool PoisonedWine::finish(const vector<Cluster> &chunk)
{
  for (auto c : chunk)
  {
	if (c.numUncertain > 0) return false;
  }
  return true;
}

double PoisonedWine::nCkLog(const int n, const int k)
{
  return (logAccum[n] - logAccum[n - k]) - (logAccum[k]);
}

void PoisonedWine::initialize(const int _numBottles, const int _testStrips,
							  const int _testRounds, const int _numPoison)
{
  env = Environment(_testRounds, _numPoison, _numBottles, _testStrips);

  logAccum[0] = 0;
  for (int i = 1; i <= 10000; i++)
  {
	logAccum[i] = log(i);
  }
  for (int i = 1; i <= 10000; i++)
  {
	logAccum[i] += logAccum[i - 1];
  }
}

void PoisonedWine::throwStrips(const vector<Cluster> &ranges, vector<int> &results)
{
  vector<string> tests(ranges.size());
  results.clear();
  results.resize(ranges.size());
  
  for (int j = 0; j < ranges.size(); j++)
  {
	int start = ranges[j].from;
	int end = ranges[j].to;
	if (debug) cerr << "(s, e] = (" << start << ", " << end << "]" << endl;
	
	stringstream ss;
	ss << start;
	for (int i = start; i < end; i++)
	{
	  ss << "," << i;
	}
	ss >> tests[j];
  }
  if (debug) cerr << "Round end" << endl;
  vector<int> ret = PoisonTest::useTestStrips(tests);
  copy(ret.begin(), ret.end(), results.begin());
}

// ver1. bpsを固定した時，stripを再利用しない場合の評価値期待値
double PoisonedWine::evaluate(const Cluster &c, const int bps, const int restRound,
							  const int depth, const Environment &e, const int clusterNum)
{
  if (restRound == 0 || depth == 2) return 0.0;
  
  // 1つしかclusterがない場合
  if (e.numUncertain == c.numUncertain)
  {
	const double prob = exp(nCkLog(c.size() - e.numPoison, bps) - nCkLog(c.size(), bps));

	double expectNext = 0.0;
	
	// 再帰part. (1) 1つ以上毒が入っているclusterを使い周す, 
	const int nextClusterNum = round((1 - prob) * clusterNum);
	if (nextClusterNum > 0)
	{
	  Environment next(e);
	  next.testStrips -= nextClusterNum;
	  next.numUncertain -= bps * (e.testStrips - nextClusterNum) + nextClusterNum;
	  next.numDetectedPoison += nextClusterNum;
	  next.numNonPoison += bps * (e.testStrips - nextClusterNum);
	  
	  int nextBps;
	  double nextEval;
	  tie(nextBps, nextEval) = selectBps(c, nextClusterNum, restRound - 1, next, depth + 1);

	  expectNext = max(expectNext, sqrt(nextEval));
	}
	
	// (2) restScript全部消費した時の残り
	if (e.testStrips * bps < c.size())
	{
	  Cluster nc(0, c.size() - e.testStrips * bps);
	  nc.numPoison = (c.numPoison == 0 || nextClusterNum > 0) ? 0 : 1;
	  nc.numUncertain = nc.size() - nc.numPoison;
		
	  Environment next(e);
	  next.testStrips -= nextClusterNum;
	  next.numUncertain -= bps * (e.testStrips - nextClusterNum) + nextClusterNum;
	  next.numDetectedPoison += nextClusterNum;
	  next.numNonPoison += bps * (e.testStrips - nextClusterNum);
	  
	  int nextBps;
	  double nextEval;
	  tie(nextBps, nextEval) = selectBps(nc, clusterNum, restRound - 1, next, depth + 1);

	  expectNext = max(expectNext, sqrt(nextEval));
	}	
	const double nonPoison = bps + expectNext;

	// scoreの二乗和
	return prob * nonPoison * nonPoison;
  }
  else
  {
	double sum = 0.0;
	double probSuccess = 0.0;
	
	const int pEnd = min(bps, c.numPoison + e.numPoison - e.numDetectedPoison);
	
	for (int p = c.numPoison; p <= pEnd; p++)
	{
	  // 全体でp個毒が入っているが，uncertainの中からq個毒を選択した計算にする
	  const int q = p - c.numPoison;

	  const double probAllToCluster =
		exp(nCkLog(e.numUncertain - e.numPoison + e.numDetectedPoison, c.numUncertain - q) +
			nCkLog(e.numPoison - e.numDetectedPoison, q) -
			nCkLog(e.numUncertain, c.numUncertain));
	  
	  sum += probAllToCluster;
	  
	  if (c.size() - p >= bps)
	  {
		const double prob = probAllToCluster * exp(nCkLog(c.size() - p, bps) - nCkLog(c.size(), bps));
		probSuccess += prob;
	  }
	}
	probSuccess /= sum;

	double expectNext = 0.0;
	// 再帰part. (1) 1つ以上毒が入っているclusterを使い周す, 
	const int nextClusterNum = round((1 - probSuccess) * clusterNum);
	if (nextClusterNum > 0)
	{
	  Environment next(e);
	  next.testStrips -= nextClusterNum;
	  next.numUncertain -= bps * (e.testStrips - nextClusterNum) + nextClusterNum;
	  next.numDetectedPoison += nextClusterNum;
	  next.numNonPoison += bps * (e.testStrips - nextClusterNum);
	  
	  int nextBps;
	  double nextEval;
	  tie(nextBps, nextEval) = selectBps(c, nextClusterNum, restRound - 1, next, depth + 1);

	  expectNext = max(expectNext, sqrt(nextEval));
	}
	
	// (2) restScript全部消費した時の残り
	if (e.testStrips * bps < c.size())
	{
	  Cluster nc(0, c.size() - e.testStrips * bps);
	  nc.numPoison = (c.numPoison == 0 || nextClusterNum > 0) ? 0 : 1;
	  nc.numUncertain = nc.size() - nc.numPoison;
		
	  Environment next(e);
	  next.testStrips -= nextClusterNum;
	  next.numUncertain -= bps * (e.testStrips - nextClusterNum) + nextClusterNum;
	  next.numDetectedPoison += nextClusterNum;
	  next.numNonPoison += bps * (e.testStrips - nextClusterNum);
	  
	  int nextBps;
	  double nextEval;
	  tie(nextBps, nextEval) = selectBps(nc, clusterNum, restRound - 1, next, depth + 1);

	  expectNext = max(expectNext, sqrt(nextEval));
	}
	
	double ans = 0.0;
	{
	  double sum = 0.0;
	  const int pEnd = min(bps, c.numPoison + e.numPoison - e.numDetectedPoison);
	  
	  for (int p = c.numPoison; p <= pEnd; p++)
	  {
		// 全体でp個毒が入っているが，uncertainの中からq個毒を選択した計算にする
		const int q = p - c.numPoison;
		
		const double probAllToCluster =
		  exp(nCkLog(e.numUncertain - e.numPoison + e.numDetectedPoison, c.numUncertain - q) +
			  nCkLog(e.numPoison - e.numDetectedPoison, q) -
			  nCkLog(e.numUncertain, c.numUncertain));
		
		sum += probAllToCluster;
		
		if (c.size() - p >= bps)
		{
		  const double prob = probAllToCluster * exp(nCkLog(c.size() - p, bps) - nCkLog(c.size(), bps));
		  const double eval = prob * (bps + expectNext) * (bps + expectNext);
		  ans += eval;
		}
	  }
	  ans /= sum;
	}
	return ans;
  }
}

/** 同一サイズのclusterがあれば，同時に評価する **/
pair<int, double> PoisonedWine::selectBps(const Cluster &c, const int clusterNum, const int restRound, const Environment &e, const int depth)
{
  if (clusterNum == 0 || restRound == 0) return make_pair(-1, 0);
  
  const int maxBps = max(1, min(c.size() - 1, ((c.size() + e.testStrips - 1) / e.testStrips) * clusterNum));
  double bestEval = -1;
  double bestBps = -1;

  for (int bps = 1; bps <= maxBps; bps++)
  {
	const double eval = evaluate(c, bps, restRound, depth, e, clusterNum);
	if (eval > bestEval)
	{
	  bestEval = eval;
	  bestBps = bps;
	}
  }
  return make_pair(bestBps, bestEval);
}

vector<int> PoisonedWine::testWine(int _numBottles, int _testStrips, int _testRounds, int _numPoison)
{
  cerr << "==============================" << endl;
  cerr << "numBottle: " << _numBottles << endl;
  cerr << "testStrips: " << _testStrips << endl;
  cerr << "testRounds: " << _testRounds << endl;
  cerr << "numPoison: " << _numPoison << endl;
  cerr << "==============================" << endl;
  
  initialize(_numBottles, _testStrips, _testRounds, _numPoison);

  vector<Cluster> chunk;
  chunk.push_back(Cluster(0, env.numBottles));
  
  for (int round = 0; round < env.testRounds; round++)
  {
	if (env.testStrips == 0 || finish(chunk)) break;

	// set up parameter
	env.numUncertain = 0;
	env.numDetectedPoison = 0;
	env.numNonPoison = 0;

	for (auto &v : chunk)
	{
	  env.numUncertain += v.numUncertain;
	  env.numDetectedPoison += v.numPoison;
	}
	for (auto &v : nonPoisonClusters)
	{
	  env.numNonPoison += v.size();
	}
	
	sort(chunk.begin(), chunk.end());
	// splitした結果，sizeが0になったclusterを削除
	// size順にsortされた時に回収
	while (!chunk.empty() && chunk.back().size() == 0)
	{
	  chunk.pop_back();
	}
	
	for (int i = 0; i < chunk.size();)
	{
	  const int start = i;
	  while (i < chunk.size() && chunk[start] == chunk[i])
	  {
		i++;
	  }
	  int bps;
	  double eval;
	  tie(bps, eval) = selectBps(chunk[start], i - start, env.testRounds - round, env, 0);
	  if (debug) cerr << "bps = " << bps << ", eval = " << eval << endl;
	  for (int j = start; j < i; j++)
	  {
		chunk[j].eval = eval;
		chunk[j].bps = bps;
	  }
	}
	
	sort(chunk.begin(), chunk.end(), [](const Cluster &c1, const Cluster &c2){ return c1.eval > c2.eval; });
	
	int restChunk = 0;
	vector<Cluster> newChunk;
	int count = 0;
	for (int i = 0; i < chunk.size() && count < env.testStrips;)
	{
	  const int start = i;
	  while (i < chunk.size() && count + (i - start) <= env.testStrips && chunk[start] == chunk[i])
	  {
		i++;		
	  }
	  const int numStrip = min((i - start) * ((chunk[start].size() + chunk[start].bps - 1) / chunk[start].bps), env.testStrips - count);

	  for (int j = start; j < i; j++)
	  {
		const int num = numStrip * ((j + 1) - start) / (i - start) - numStrip * (j - start) / (i - start);
		if (num > 0)
		{
		  chunk[j].split(chunk[j].bps, num, newChunk, j);
		}
	  }
	  count += numStrip;	  
	}
	vector<int> results;
	throwStrips(newChunk, results);

	int lostStrips = 0;
	for (int i = 0; i < newChunk.size(); i++)
	{
	  if (results[i] == NO_POISON)
	  {
		newChunk[i].numPoison = 0;
		newChunk[i].numUncertain = 0;
		nonPoisonClusters.push_back(newChunk[i]);
	  }
	  else
	  {
		newChunk[i].numPoison = 1;
		newChunk[i].numUncertain = newChunk[i].size() - 1;
		chunk.push_back(newChunk[i]);

		if (chunk[newChunk[i].parentIndex].numPoison == 1)
		{
		  chunk[newChunk[i].parentIndex].numPoison = 0;
		}
		lostStrips++;
	  }
	}
	env.testStrips -= lostStrips;
  }

  vector<int> ret;
  for (auto v : chunk)
  {
	for (int i = v.from; i < v.to; i++)
	{
	  ret.push_back(i);
	}
  }
  cerr << "nonPoison = " << env.numBottles - ret.size() << "(rate = " << static_cast<double>(env.numBottles - ret.size()) / (env.numBottles - env.numPoison)  << ")" << endl;
  
  return ret;
}
// -------8<------- end of solution submitted to the website -------8<-------

#ifdef TEST
int main()
{
  PoisonedWine pw;
  int numBottles, testStrips, testRounds, numPoison;
  cin >> numBottles >> testStrips >> testRounds >> numPoison;
    
  vector<int> ret = pw.testWine(numBottles, testStrips, testRounds, numPoison);
  cout << ret.size() << endl;
  for (int i = 0; i < (int)ret.size(); ++i)
	cout << ret[i] << endl;
  cout.flush();
}
#endif
